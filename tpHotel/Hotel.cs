﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class hotel
    {
        private int id;
        private string nom;
        private string adresse;
        private string ville;
       
        public hotel(int UneId,string UnNom, string UneAdresse, string UneVille)
        {
            nom = UnNom;
            id = UneId;
            adresse = UneAdresse;
            ville = UneVille; 
        }

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
}
