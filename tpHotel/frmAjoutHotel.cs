﻿using System;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutHotel : Form
    {
        string nom;
        string ville;
        string adresse;
        public frmAjoutHotel()
        {
            InitializeComponent();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.raz();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            nom = txtNom.Text;
            adresse = txtAdresse.Text;
            ville = txtVille.Text;
            Persistance.ajouteHotel(txtNom.Text, txtAdresse.Text, txtVille.Text);

            MessageBox.Show("L'hotel " + nom +" "+ adresse +" "+ ville + "as été crée");
        }

        private void raz()
        {
            this.txtNom.Text = "";
            this.txtAdresse.Text = "";
            this.txtVille.Text = "";
            this.txtNom.Focus();
        }
    }
}
